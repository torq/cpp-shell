#ifndef CPPSH_CORE
#define CPPSH_CORE

#include <string>
#include <vector>
#include <map>

#include "commands/command.cpp"
#include "commands/cd-command.cpp"
#include "commands/help-command.cpp"
#include "commands/exit-command.cpp"

class Shell {
private:
    static constexpr int rl_buf_s = 1024;
    static constexpr int tok_buf_s = 64;
    std::map<std::string, Command*> builtin_map;

public:
    Shell();
    ~Shell();

    void run();

private:
    bool launch(const std::vector<std::string>&);
    bool execute(const std::vector<std::string>&);
};

#endif
