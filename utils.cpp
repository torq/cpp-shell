#include "utils.hpp"

#include <cstdlib>
#include <iostream>
#include <regex>

std::string utils::read_line()
{
    std::string line;
    std::getline(std::cin, line);

    return line;
}

std::vector<std::string> utils::split_line(const std::string& line)
{
	std::regex re("[ \t\r\n\a]");
	std::sregex_token_iterator first {line.begin(), line.end(), re, -1}, last;
	return std::vector<std::string>(first, last);
}