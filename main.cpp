#include <iostream>
#include "shell.hpp"

int main(int argc, char **argv)
{
	// Carregar arquivos de configuração
	// TODO: Permitir utilização de arquivo de configuração

	// Executar comando de loop
	Shell shell;
	shell.run();

	// Desligar
	std::cout << "Obrigado por usar CPPSH.\n";
}