CC=g++
CC_OPT=-Wall -std=c++11
EXE=shell.elf

all: main.o
	$(CC) $(CC_OPT) -o shell.elf main.o shell.o utils.o command.o exit-command.o help-command.o cd-command.o
	make clean

main.o: shell.o main.cpp
	$(CC) $(CC_OPT) -c main.cpp 

shell.o: shell.hpp shell.cpp utils.o cd-command.o help-command.o exit-command.o command.o
	$(CC) $(CC_OPT) -c shell.cpp

utils.o: utils.hpp utils.cpp
	$(CC) $(CC_OPT) -c utils.cpp

cd-command.o: command.o commands/cd-command.cpp
	$(CC) $(CC_OPT) -c "commands/cd-command.cpp"	

help-command.o: command.o commands/help-command.cpp
	$(CC) $(CC_OPT) -c "commands/help-command.cpp"

exit-command.o: command.o commands/exit-command.cpp
	$(CC) $(CC_OPT) -c "commands/exit-command.cpp"

command.o: commands/command.cpp
	$(CC) $(CC_OPT) -c commands/command.cpp

run: all
	./$(EXE)

clean:
	rm *.o