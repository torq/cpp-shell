#ifndef CPPSH_UTILS
#define CPPSH_UTILS

#include <vector>
#include <string>

namespace utils {
    std::string read_line();
    std::vector<std::string> split_line(const std::string& line);
};

#endif