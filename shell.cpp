#include "shell.hpp"

#include <iostream>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>
#include <cerrno>

#include "utils.hpp"

static void launch_child(const std::vector<std::string>& args);
static void launch_parent(pid_t pid);

Shell::Shell()
{
	builtin_map["cd"] = new Cd_Command();
	builtin_map["help"] = new Help_Command();
	builtin_map["exit"] = new Exit_Command();
}

Shell::~Shell()
{
	for(auto pair : builtin_map)
		delete pair.second;
}

void Shell::run()
{
	bool status;

	do
	{
		std::cout << "> ";

		auto line = utils::read_line();
		auto args = utils::split_line(line);
		status = execute(args);
	} while(status);
}

bool Shell::launch(const std::vector<std::string>& args)
{
	pid_t pid = fork();
	if (!pid)
		launch_child(args);
	else if (pid < 0) //Erro ao fazer o forking
		std::cerr << strerror(errno) << std::endl;
	else
		launch_parent(pid);

	return true;
}

static void launch_child(const std::vector<std::string>& args)
{
	auto args_size = args.size();
	auto args_ptr = new const char* [args_size];
	for(unsigned i=0; i<args_size; ++i)
	{
		args_ptr[i] = (args[i].empty())
			? NULL
			: args[i].c_str();
	}

	if (execvp(args[0].c_str(), (char **)args_ptr) == -1)
		std::cerr << strerror(errno) << std::endl;

	delete[] args_ptr;

	exit(EXIT_FAILURE);
}

static void launch_parent(pid_t pid)
{
	int status;
	do
	{
		waitpid(pid, &status, WUNTRACED);
	} while(!WIFEXITED(status) && !WIFSIGNALED(status));
}

bool Shell::execute(const std::vector<std::string>& args)
{
	if (args[0].empty())
		return true;
	
	try
	{
		return builtin_map.at(args[0])->execute(args);
	} catch (const std::out_of_range& oor)
	{
		return launch(args);
	}
}