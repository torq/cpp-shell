#ifndef CPPSH_COMMAND
#define CPPSH_COMMAND

#include <vector>
#include <string>

class Command
{
public:
    virtual bool execute(std::vector<std::string> args) = 0;
};

#endif