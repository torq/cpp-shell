#ifndef CPPSH_EXIT_COMM
#define CPPSH_EXIT_COMM

#include <vector>
#include <string>

#include "command.cpp"

class Exit_Command : public Command
{
public:
    bool execute(std::vector<std::string> args)
    {
        return false;
    }
};

#endif