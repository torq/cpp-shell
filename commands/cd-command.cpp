#ifndef CPPSH_CD_COMM
#define CPPSH_CD_COMM

#include <vector>
#include <string>
#include <iostream>
#include <cerrno>
#include <cstring>
#include <unistd.h>

#include "command.cpp"

class Cd_Command : public Command
{
public:
    bool execute(std::vector<std::string> args)
    {
        if(args[1].empty())
            std::cerr << "cppsh: expected argument to \"cd\"" << std::endl;
        else if (chdir(args[1].c_str()))
            std::cerr << strerror(errno) << std::endl;

        return true;
    }
};

#endif